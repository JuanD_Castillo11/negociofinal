package logica;

<<<<<<< HEAD
public class CreditoHipotecario {
	private double montoPrestamo;
    private double tasaInteres;
	
	    public CreditoHipotecario(Cliente cliente, double montoPrestamo, double tasaInteres) {
	        super();
	        this.montoPrestamo = montoPrestamo;
	        this.tasaInteres = tasaInteres;
	    }

	    
	    public double calcularCuotaMensual(int plazoMeses) {
	        double tasaMensual = tasaInteres / 12;
	        double cuotaMensual = montoPrestamo * (tasaMensual / (1 - Math.pow(1 + tasaMensual, -plazoMeses)));
	        return cuotaMensual;
	   }
}
=======

	public  abstract class CreditoHipotecario extends Cuenta {
		private double montoPrestamo;
	    private double tasaInteres;
		
		    public CreditoHipotecario(int numero, double saldo, double tasaInteres, Cliente cliente, double montoPrestamo) {
		        super(numero, tasaInteres, tasaInteres, cliente);
		        this.montoPrestamo = montoPrestamo;
		        this.tasaInteres = tasaInteres;
		    }

		    
		    public double calcularCuotaMensual(int plazoMeses) {
		        double tasaMensual = tasaInteres / 12;
		        double cuotaMensual = montoPrestamo * (tasaMensual / (1 - Math.pow(1 + tasaMensual, -plazoMeses)));
		        return cuotaMensual;
		   }
	}
>>>>>>> a21f60e61e85febcd4c8b7b2ec5c6eacd5f7c48c
