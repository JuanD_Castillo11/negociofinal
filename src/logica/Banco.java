package logica;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

import Persistencia.Archivo;

import Persistencia.Archivo;

public class Banco {
    private ArrayList<Cuenta> cuentas;
    private HashMap<Integer, Cliente> clientes;
	private String nombreArchivos;

    public ArrayList<Cuenta> getCuentas() {
        return cuentas;
    }

    public HashMap<Integer, Cliente> getClientes() {
        return clientes;
    }

    public Banco() {
        this.cuentas = new ArrayList<Cuenta>();
        this.clientes = new HashMap<Integer, Cliente>();
    }

    public void crearCliente(int id, String nombre, String apellido) throws Exception {
        if (this.clientes.containsKey(id)) {
            throw new Exception("Cliente repetido");
        } else {
            Cliente cliente = new Cliente(id, nombre, apellido);
            this.clientes.put(id, cliente);
        }
    }

    public void crearCuenta(int numero, double saldo, String tipo, int idCliente) throws Exception {
        if (!this.clientes.containsKey(idCliente)) {
            throw new Exception("Cliente no existente");
        } else if (this.existeCuenta(numero)) {
            throw new Exception("La cuenta ya existe");
        } else if (this.existeTipoCuenta(idCliente, tipo)) {
            throw new Exception("El cliente ya tiene una cuenta de tipo : " + tipo);
        } else {
<<<<<<< HEAD
           
        }     
      }
         
=======
            Cliente cliente = this.clientes.get(idCliente);
            Cuenta cuenta = null ;
            switch (tipo) {
                case "Ahorros":
                    cuenta = new CuentaAhorros(numero, saldo, saldo, cliente); // Create an instance of CuentaAhorrosNormal
                    break;
                case "Corriente":
                    cuenta = new CuentaCorriente(numero, saldo,cliente, saldo);
                    break;
                case "TarjetaCredito":
                    cuenta = new TarjetaCredito(numero, saldo, saldo, cliente);
                    break;
                case "CreditoHipotecario":
                    // You need to provide the necessary parameters to create a CreditoHipotecario
                    // cuenta = new CreditoHipotecario(...);
                    break;
                case "CreditoLibreInversion":
                    // You need to provide the necessary parameters to create a CreditoLibreInversion
                    // cuenta = new CreditoLibreInversion(...);
                    break;
                default:
                    throw new Exception("Tipo de cuenta no válido");
            }
            this.cuentas.add(cuenta);
            cliente.AgregarCuenta(cuenta);
        }
    }


>>>>>>> a21f60e61e85febcd4c8b7b2ec5c6eacd5f7c48c
    private boolean existeTipoCuenta(int idCliente, String tipo) {
        for (Cuenta cuenta : this.clientes.get(idCliente).getCuentas()) {
            if (cuenta.getClass().getSimpleName().equals(tipo)) {
                return true;
            }
        }
        return false;
    }

    public boolean existeCuenta(int numero) {
        for (Cuenta cuenta : this.cuentas) {
            if (Cuenta.getNumero() == numero) {
                return true;
            }
        }
        return false;
    }

    public void almacenarTP() {
<<<<<<< HEAD
        ArrayList<String> lineasClientes = new ArrayList<String>();
        for (Map.Entry<Integer, Cliente> entry : this.clientes.entrySet()) {
            Cliente cliente = entry.getValue();
            lineasClientes.add(cliente.getnumero() + ";" + cliente.getNombre() + ";" + cliente.getApellido());
        }
        Archivo.almacenar("clientes.csv", lineasClientes);

        ArrayList<String> lineasCuentas = new ArrayList<String>();
        for (Cuenta cuenta : this.cuentas) {
        }
    }

	public ArrayList<String> cargarTP() {
		// TODO Auto-generated method stub
		ArrayList<String> lineas = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader("nombreArchivos"));
			String linea;
			while((linea=br.readLine()) != null) {
				lineas.add(linea);				
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lineas;
	}

	public String getNombreArchivos() {
		return nombreArchivos;
	}

	public void setNombreArchivos(String nombreArchivos) {
		this.nombreArchivos = nombreArchivos;
	}
}
   
=======
		ArrayList<String> lineasClientes = new ArrayList<String>();
		for(Integer id : this.clientes.keySet()) {
			Cliente cliente = this.clientes.get(id);
			lineasClientes.add(cliente.getId() + ";" + cliente.getNombre() + ";" + cliente.getApellido());
		}
		Archivo.almacenar("clientes.csv", lineasClientes);
		
		ArrayList<String> lineasCuentas = new ArrayList<String>();
		for(Cuenta cuenta : this.cuentas) {
			lineasCuentas.add(cuenta.getNumero() + ";" + cuenta.getSaldo() + ";" + cuenta.getTipo() + ";" + cuenta.getCliente().getId());
		}
		Archivo.almacenar("cuentas.csv", lineasCuentas);
	}

    public void cargarTP() throws Exception {
        ArrayList<String> lineasClientes = Archivo.cargar("clientes.csv");
        for(String linea : lineasClientes) {
            String datos[] = linea.split(";");
            this.crearCliente(Integer.parseInt(datos[0]), datos[1], datos[2]);
        }
        ArrayList<String> lineasCuentas = Archivo.cargar("cuentas.csv");
        for(String linea : lineasCuentas) {
            String datos[] = linea.split(";");
            this.crearCuenta(Integer.parseInt(datos[0]), Double.parseDouble(datos[1]), datos[2], Integer.parseInt(datos[3]));
        }
    }
}
>>>>>>> a21f60e61e85febcd4c8b7b2ec5c6eacd5f7c48c
